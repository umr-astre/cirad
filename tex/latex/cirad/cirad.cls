% Cirad institutional LaTeX package
% Copyright (c) Facundo Muñoz  2017
% LaTeX Project Public License v1.3c
%
% Adapted from:
% https://www.overleaf.com/latex/templates/papier-a-en-tete-icube/kxcxzgnndzrt
% Infos et bugs : vincent.mazet@unistra.fr

% Pass options to documentclass
% see Sec. 5.1 in
% http://tug.org/pracjourn/2005-4/hefferon/hefferon.pdf
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\ProcessOptions \relax
\LoadClass{scrartcl}

\RequirePackage{graphicx,grffile,color}
\RequirePackage{ifthen}
\RequirePackage{fancyhdr}
\RequirePackage[colorlinks, urlcolor=blue]{hyperref}
% TODO:
% Class scrartcl Warning: Usage of package `fancyhdr' together
% (scrartcl)              with a KOMA-Script class is not recommended.
% (scrartcl)              I'd suggest to use 
% (scrartcl)              package `scrlayer-scrpage'.
% (scrartcl)              Nevertheless, using requested
% (scrartcl)              package `fancyhdr' on input line 14.
% e.g. https://tex.stackexchange.com/questions/192902/how-do-i-insert-the-section-name-into-the-header

% \usepackage{helvet}


% define colors and commands for including logos
\RequirePackage{cirad}

% page layout
\voffset -1in
\topmargin 10mm
\headheight 5mm
\headsep 35mm
\textheight 210mm
\footskip 20mm
\hoffset -1in
\oddsidemargin 50mm
\evensidemargin 50mm
\textwidth 140mm
\marginparsep -180mm
\marginparwidth 35mm
\parindent 0mm


\fancypagestyle{plain}{%
  \fancyhf{}%
  \lhead{\hspace*{-40mm}\ciraden}
}


\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}

% Police sans-serif
% \renewcommand{\familydefault}{\sfdefault}


% % Marge (expéditeur, suivi, adresse)
% \newcommand{\marge}{%
%   \vspace*{-225mm}\hspace*{-40mm}\vbox{
%     \vtop to  45mm {\vfil\usebox{\exped}\vfil}
%     \vbox to  30mm {\vfil}
%     \vbox to  30mm {\vfil\usebox{\suiv}\vfil}
%     \vbox to 100mm {\vfil}
%     \vbox to  20mm {\vfil\usebox{\adress}}
%   }
% }

% % Tutelles
% \newcommand{\tutelles}{
%   \begin{tabular}{@{}r@{}}%
%   \scriptsize Sous la co-tutelle de \\%
%   \hline%
%   ~\\[-3mm]%
%   \includegraphics[height=1cm]{cnrs.eps}\quad%
%   \ifthenelse{\value{engees}=1}{\includegraphics[height=1cm]{engees.eps}\quad}{}%
%   \ifthenelse{\value{insa}=1}{\includegraphics[height=1cm]{insa.eps}\quad}{}%
%   \includegraphics[height=1cm]{unistra.eps}%
%   \end{tabular}
% }

% Éléments de la lettre
\lhead{\hspace*{-40mm}\ifthenelse{\value{page}=1}{\ciraden}{\cirad}}      % Logo Cirad
\chead{}                                                                    %
\rhead{}                                                                    %
% \lfoot{\ifthenelse{\value{page}=1}{\marge}{}}                               % Expéditeur, suivi, adresse
\cfoot{}                                                                    %
% \rfoot{\ifthenelse{\value{page}=1}{\tutelles}{\raisebox{-5mm}{\thepage}}}   % Tutelles

% % Expéditeur
% \newsavebox{\exped}
% \newcommand{\expediteur}[3]{\savebox{\exped}{\parbox{50mm}{%
%   \textcolor{bleu}{#1 \textbf{#2}} \\[1ex]\scriptsize#3}}}

% % Suivi de l'affaire
% \newsavebox{\suiv}
% \newcommand{\suivi}[1]{\savebox{\suiv}{\parbox{35mm}{\scriptsize%
%   \textcolor{bleu}{Affaire suivie par~:} \\[.5ex]#1}}}

% % Adresse du laboratoire
% \newsavebox{\adress}
% \newcommand{\adresse}[1]{\savebox{\adress}{\parbox{50mm}{%
%   \scriptsize\color{gris}\textbf{Laboratoire ICube --- UMR 7357}\\#1}}}

% % Autres commandes
% \newcommand{\lieudate}[1]{\rhead{\ifthenelse{\value{page}=1}{#1}{}}}
% \newcounter{engees}
% \newcommand{\logoengees}{\setcounter{engees}{1}}
% \newcommand{\nologoengees}{\setcounter{engees}{0}}
% \newcounter{insa}
% \newcommand{\logoinsa}{\setcounter{insa}{1}}
% \newcommand{\nologoinsa}{\setcounter{insa}{0}}
% \newcommand{\destinataire}[1]{\vspace*{-20mm}\vbox to 4cm {\vfil \hfill\hbox to 100mm{\parbox{90mm}{#1}}\vfil}\vspace*{20mm}}

