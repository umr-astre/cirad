# Cirad institutional LaTeX package


This LaTeX package ships Cirad's institutional logos and colors.
It also includes a class file based on KOMAscript's `scrartcl` that uses `fancyhdr` to include the logos in the pages.

## Installation

On a Unix-like system, download and uncompress somewhere, and then type `make` from the command line. Then, update the TeX database with `texhash ~/texmf`.

On MS Windows, if you are using the template for RMarkdown, the easiest is to use [`tinytex`](https://yihui.org/tinytex/) as follows:

- Install the [`tinytex`](https://yihui.org/tinytex/) R package. From R: `install.packages('tinytex')`

- Use it to install the TinyTeX LaTeX distribution. From R: `tinytex::install_tinytex()`

<!-- - Install some more required LaTeX packages. From R: `tinytex:::install_yihui_pkgs()` -->

<!-- Basically, palatino, that could be installed manually with `tinytex::tlmgr_install("palatino")`, but does not fix the error of Palatino not found-->

- [Download](https://forgemia.inra.fr/umr-astre/cirad/-/archive/master/cirad-master.zip) the LaTeX pacakge `cirad`.

- From R: 

    ```r
    unzip('~/../Downloads/cirad-master.zip')
    file.copy(
        'cirad-master/tex/latex/cirad',
        to = file.path(tinytex::tinytex_root(), 'texmf-local/tex/latex'),
        recursive = TRUE
    )
    ```
<!--
- You will also need the font _Palatino_, which is used in the template. You will typically have it in your windows installation (e.g. `C:\WINDOWS\FONTS\PALA.TTF`). To make it accessible from R, import your system's fonts using the package `extrafont` as follows:

    - `install.packages('extrafont'); library(extrafont); import_fonts()"

    - If you find that fonts are not being correctly installed due to a [issue "No FontName. Skipping"](https://github.com/wch/extrafont/issues/89), then use the workaround described there to downgrage the pacakge `Rttf2p1` as explained therein. Note that you will need [Rtools](https://cran.r-project.org/) installed.

    - Still, this doesn't seem to solve the issue with Palatino. Otherwise, remove Palatino from `src/preamble.tex`.

    - Actually, this LaTeX package does not require Palatino at all. It is the project template who does `\setmainfont{Palatino}` in `src/preamble.tex`. It seems that in Windows you need to specify `Palatino Linotype` in the call instead. Then it works.
-->

## Example

![example](https://i.imgur.com/CyGqhbR.png)

## License

Copyright (c) Facundo Muñoz 2017

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3c
of this license or (at your option) any later version.
The latest version of this license is in
http://www.latex-project.org/lppl.txt
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.
